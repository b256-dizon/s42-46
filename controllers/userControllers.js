const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/product.js");

// Check if email exist in database
module.exports.checkIfEmailExists = (requestBody) => {

	return User.find({email: requestBody.email}).then(result => {

		if(result.length > 0) {

			return true;

		} else {

			return false;

		}
	})
};

// Register User
module.exports.registerUser = (requestBody) => {
	let newUser = new User({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err) {

			return false;

		} else {

			return true;

		}
	})
} 

// All users
module.exports.allUsers = () => {
	return User.find({}).then(result => {
		result.password = " ";
		return result;
	})
}

// User Authentication
module.exports.authenticateUser = (requestBody) => {
	return User.findOne({email: requestBody.email}).then(result => {
		if(result == null) {

			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {

				return false;
			};
		};
	});
};

// Checkout
	module.exports.checkout = async (reqbody) => {
	let price;
	let name = await Product.findById(reqbody.prodId).then(result => {
		let increment;
		price = result.price;
		if (result.userOrders.length == 0) {
			increment = 1;
		} else {
			increment = result.userOrders.length + 1;
		}

		let userOrder = { 
				userId: reqbody.userId,
				orderId: increment,
				email : reqbody.email
			}

		result.userOrders.push(userOrder);
		result.save().then((result, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		})
		return result.name;
	});
	let order = {
		products : [{
			productId : reqbody.prodId,
			productName : name,
			quantity : reqbody.quantity
		}],
		totalAmount : reqbody.quantity * price,
	}
	return await User.findById(reqbody.userId).then(result => {
		result.orderedProduct.push(order);
		return result.save().then((result, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		})
	})
};







// Get User orders

module.exports.getOrder = (paramsId) => {
	let order = User.findById(paramsId.userId).then(result => {
		let userOrder = {
			id: result._id,
			Email : result.email,
			Orders : []
		}
		let orders = result.orderedProduct.forEach(i => {
			let prod = {
				Product : i.products[0].productName,
				quantity : i.products[0].quantity,
				Total : i.totalAmount,
			}
			userOrder.Orders.push(prod);
		})
		if (result.isAdmin) {
			return false;
		} else {
			return userOrder;
		}
	})
	if (order == false) {
		return false;
	} else {
		return order;
	}
}

module.exports.setAdmin = (reqbody) => {
	return User.findOneAndUpdate(reqbody, {isAdmin: true}).then((result, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	})
}


