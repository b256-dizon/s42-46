const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");


// Register a product
router.post("/create", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin) {
		productController.addProduct(data.product).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});

//  Get all active products
router.get("/active", (req, res) => {
	productController.Availableproducts().then(resultFromController => res.send(resultFromController));
})

// Get all product list
router.get("/all", (req, res) => {
	productController.Allproducts().then(resultFromController => res.send(resultFromController));
})


// get product using ID
router.get("/:Id", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})


// Update the product using ID
router.put("/update/:Id", auth.verify, (req, res) => {
	const data = {
		prod: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {
		productController .updateProduct(data.params, data.prod).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

// Delete product using ID
router.patch("/archive/:Id", auth.verify, (req, res) => {

    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        params: req.params
    }

    if (data.isAdmin) {
        productController.archiveProduct(data.params, req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send(false);
    }

})

module.exports = router;


