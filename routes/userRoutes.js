const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js");


// check if email already exist in the database
router.post("/checkEmail", (req, res) => {

	userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController));

});

// Register a User

router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

})

// Retrieve all users
router.get("/allUsers", (req, res) => {
	userController.allUsers().then(resultFromController => res.send(resultFromController));
})


// Login user
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));
	})

// Checkout


// Checkout

router.post("/checkout", auth.verify, (req, res) => {
	const data = {
		prodId : req.body.prod,
		quantity : req.body.quantity,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		email : auth.decode(req.headers.authorization).email,
		userId : auth.decode(req.headers.authorization).id
	}
	if (data.isAdmin == false) {
		userController.checkout(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})



router.put("/admin", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin) {
		userController.setAdmin(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})

// Get order
router.get("/order/:userId", (req, res) => {
	userController.getOrder(req.params).then(resultFromController => res.send(resultFromController));
})
module.exports = router;

//set Admin
router.put("/admin", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin) {
		userController.setAdmin(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
})
